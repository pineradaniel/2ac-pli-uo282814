#define NROWS    16384 // 2^13 rows
#define NCOLS    16384 // 2^13 cols
#define NTIMES   30   // Repeat 10 times
// Matrix size 2^26 = 64 MiBytes
char matrix[NROWS][NCOLS];

int main(void)
{
    int i, j, rep;

    for (rep = 0; rep < NTIMES; rep++)
    {
        for (i = 0; i < NROWS; i++)
        {
            for(j = 0; j < NCOLS; j++)
            {
                matrix[i][j] = 'A';
            }
        }
    }
}
