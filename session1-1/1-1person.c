#include <stdio.h>
#include <string.h>

struct _Person
{
    char name[30];
    int heightcm;
    double weightkg;
};

// This abbreviates the type name
typedef struct _Person Person;
typedef Person* PPerson;
int main(int argc, char* argv[])
{
    Person Javier;
    PPerson pJavier;
    Person Peter;
    strcpy(Peter.name, "Peter");
    strcpy(Javier.name, "Javier");
    pJavier=&Javier;
    Javier.heightcm=180;
    Javier.weightkg=84.0;
    pJavier->weightkg=83.2;
    Peter.heightcm = 175;
    Peter.weightkg = 65.2;
    printf("%s's height: %d cm;%s's weight: %f kg\n",Peter.name,Peter.heightcm,Peter.name,Peter.weightkg);
    printf("%s's height: %d cm;%s's weight: %f kg\n",Javier.name,Javier.heightcm,Javier.name,Javier.weightkg);

// TODO: Assign the weight

    // TODO: Show the information of the Peter data structure on the screen

    return 0;
}
